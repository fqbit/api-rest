<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'products';

    protected $fillable = [
        'name',
        'description',
        'stock',
        'price',
        'img',
        'user_id',
        'vendor_id'
    ];

    //Relaciones
    public function user(){
        return $this->belongsTo(User::class);
    }

    public function vendor(){
        return $this->belongsTo(Vendor::class);
    }

    //Scopes
    public function scopeByName($query, $name){
        return  $query->orWhere('name','like','%'. $name .'%');
    }

    public function scopeByDescription($query, $description){
        return  $query->orWhere('description','like','%'. $description .'%');
    }

    public function scopeByVendor($query, $name){
        return  $query->whereHas('vendor',function ($query) use ($name){
                    $query->where('name','like','%'. $name .'%');
        });
    }
}