<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vendor extends Model
{
    use HasFactory,SoftDeletes;

    protected $table = 'vendors';

    protected $fillable = [
        'name',
        'phone',
        'address'
    ];

    public function products(){
        return $this->hasMany(Product::class);
    }

    public function scopeByName($query, $name){
        return $query->orWhere('name','like','%'. $name .'%');
    }

}
