<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use Auth;
use App\Models\User;
use App\Http\Resources\UserResource;
use App\Http\Resources\UserCollection;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->validate([
            'name' => 'nullable|string',
            'email' => 'nullable|string'
        ]);

        $users = new User();

        if($request->has('name')){
            $users = $users->byName($request->name);
        }

        if($request->has('email')){
            $users = $users->byEmail($request->email);
        }

        $users = $users->get();

        return response()->json([
            'users' => new UserCollection($users)
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:2|max:50',
            'email' => 'required|email|unique:users,email',
            'phone' =>  'required|size:10',
            'birthdate' => 'required|date',
            'password' =>  'required|min:5'
        ]);

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->birthdate = $request->birthdate;
        $user->password = bcrypt($request->password);
        $user->save();

        return response()->json([
            'user' => $user
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        if($user === null ){
            return response()->json([
                'errors' => [ 'user' => 'Not Found' ]
            ],404);  
        }
        
        return response()->json([
            'user' => new UserResource($user)
        ],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id); 
        if($user === null ){
            return response()->json([
                'errors' => [ 'user' => 'Not Found' ]
            ],404);  
        }

        $request->validate([
            'name' => 'nullable|min:2|max:50',
            'email' => 'nullable|email|unique:users,email,'. $id,
            'phone' =>  'nullable|size:10',
            'birthdate' => 'nullable|date',
            'password' =>  'nullable|min:5'
        ]);

        $user = User::find($id); 
        if($user === null ){
            return response()->json([
                'errors' => [ 'user' => 'Not Found' ]
            ],404);  
        }
        $user->name = $request->has('name') ? $request->name : $user->name;
        $user->email = $request->has('email') ? $request->email : $user->email;
        $user->phone = $request->has('phone') ? $request->phone : $user->phone;
        $user->birthdate = $request->has('birthdate') ? $request->birthdate :  $user->birthdate ;
        $user->password = $request->has('password') ? bcrypt($request->password) : $user->password;
        $user->save();

        return response()->json([
            'user' => $user
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id); 
        if($user === null ){
            return response()->json([
                'errors' => [ 'user' => 'Not Found' ]
            ],404);  
        }
        $user->delete();

        return response()->json([
            'user' => $user
        ],200);
    }

    public function restore($id)
    {
        $user = User::withTrashed()->where('id',$id)->first(); 
        
        if($user === null ){
            return response()->json([
                'errors' => [ 'user' => 'Not Found' ]
            ],404);  
        }
        
        $user->restore();
        
        return response()->json([
            'user' => $user
        ],200);

    }    
}
