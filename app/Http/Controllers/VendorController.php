<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Vendor;
use DB;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Http\Resources\VendorCollection;
use App\Http\Resources\ResourceCollection;
class VendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->validate([
            'name' => 'nullable|string'
        ]);

        $vendors = new Vendor ();

        if($request->has('name')){
            $vendors = $vendors->byName($request->name);
        }

        $vendors = $vendors->get();

         return response()->json([
            'result' => new VendorCollection($vendors),
        ],200);        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:2|max:50|unique:vendors,name',
            'address' => 'required|min:10|max:50',
            'phone' =>  'required|size:10',
        ]);
 
        $vendor = new Vendor();
        $vendor->name = $request->name;
        $vendor->phone = $request->phone;
        $vendor->address = $request->address;
        $vendor->save();

        return response()->json([
            'vendor' => $vendor
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $vendor = Vendor::find($id);
        if($vendor === null ){
            return response()->json([
                'errors' => [ 'vendor' => 'Not Found' ]
            ],404);  
        }
        
        return response()->json([
            'vendor' => $vendor
        ],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

   

    public function update(Request $request, $id)
    {
        $vendor = Vendor::find($id); 
        if($vendor === null ){
            return response()->json([
                'errors' => [ 'vendor' => 'Not Found' ]
            ],404);  
        }

        $request->validate([
            'name' => 'required|min:2|max:50|unique:vendors,name,'. $id,
            'address' => 'required|min:10|max:50',
            'phone' =>  'required|size:10',
        ]);
         
        $vendor->name = $request->has('name') ? $request->name : $vendor->name;
        $vendor->phone = $request->has('phone') ? $request->phone : $vendor->phone;
        $vendor->address = $request->has('address') ? $request->address : $vendor->address;
        $vendor->save();

        return response()->json([
            'vendor' => $vendor
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vendor = Vendor::find($id); 

        if($vendor === null ){
            return response()->json([
                'errors' => [ 'vendor' => 'Not Found' ]
            ],404);  
        }
        $vendor->delete();

        return response()->json([
            'vendor' => $vendor
        ],200);
    }

    public function restore($id)
    {

        $vendor = Vendor::withTrashed()->where('id',$id)->first(); 

        if($vendor === null ){
            return response()->json([
                'errors' => [ 'vendor' => 'Not Found' ]
            ],404);  
        }

        $vendor->restore();
        
        return response()->json([
            'vendor' => $vendor
        ],200);
    }
}


