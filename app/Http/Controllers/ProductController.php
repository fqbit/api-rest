<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use App\Models\Product;
use App\Http\Resources\ProductResource;
use App\Http\Resources\ProductCollection;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

class ProductController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->validate([
            'name' => 'nullable|string',
            'description' => 'nullable|string',
            'vendor' => 'nullable|string'
        ]);

        $products = new product();

        if($request->has('name')){
            $products = $products->byName($request->name);
        }
        if($request->has('description')){
            $products = $products->byDescription($request->description);
        }

        if($request->has('vendor')){
            $products = $products->byVendor($request->vendor);
        }

        $products = $products->get();

        return response()->json([
            'products' =>  new ProductCollection($products),
        ],200);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required|min:2|max:50|unique:products,name',
            'description' => 'required',
            'stock' =>  'required|min:1',
            'price' => 'required|min:0.01',
            'vendor_id' => 'required',
            'img' => 'required|mimes:jpg,jpeg,bmp,png'
        ]);

        $product = new Product();
        $product->name = $request->has('name') ? $request->name : $product->name;
        $product->description = $request->has('description') ? $request->description : $product->description;
        $product->stock = $request->has('stock') ? $request->stock : $product->stock;
        $product->price = $request->has('price') ? $request->price : $product->price;
        $product->vendor_id = $request->has('vendor_id') ? $request->vendor_id : $product->vendor_id;
        $product->user_id = auth()->user()->id;

        $path = Storage::disk('public')->put('products',$request->file('img'));
        $product->img = $path;
       
        $product->save();

        return response()->json([
            'product' => new ProductResource($product)
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        //
        $product = Product::find($id);

        if($product === null ){
            return response()->json([
                'errors' => [ 'product' => 'Not Foundd' ]
            ],404);  
        }

        return response()->json([
            'product' => new ProductResource($product)
        ],200);
    }

    public function update(Request $request, $id)
    {   
        $product = Product::find($id); 

        if($product === null ){
            return response()->json([
                'errors' => [ 'product' => 'Not Found' ]
            ],404);  
        }

        $request->validate([
            'name' => 'nullable|min:2|max:50|unique:products,name,' . $id,
            'description' => 'nullable',
            'stock' =>  'nullable|min:1',
            'price' => 'nullable|min:0.01',
            'vendor_id' => 'nullable',
            'img' => 'nullable|mimes:jpg,jpeg,bmp,png'
        ]);

        $product->name = $request->has('name') ? $request->name : $product->name;
        $product->description = $request->has('description') ? $request->description : $product->description;
        $product->stock = $request->has('stock') ? $request->stock : $product->stock;
        $product->price = $request->has('price') ? $request->price : $product->price;
        $product->vendor_id = $request->has('vendor_id') ? $request->vendor_id : $product->vendor_id;
        $product->user_id = auth()->user()->id;

        if($request->hasFile('img')){
            $path = Storage::disk('public')->put('products',$request->file('img'));
            Storage::disk('public')->delete($product->img);//delete 
            $product->img = $path;
        }

        $product->save();

        return response()->json([
            'product' => new ProductResource($product)
        ],200);

    }

    public function destroy($id)
    {
        $product = Product::find($id); 

        if($product === null ){
            return response()->json([
                'errors' => [ 'product' => 'Not Found' ]
            ],404);  
        }
        $product->delete();

        return response()->json([
            'product' => $product
        ],200);
    }

    public function restore($id)
    {

        $product = Product::withTrashed()->where('id',$id)->first(); 

        if($product === null ){
            return response()->json([
                'errors' => [ 'product' => 'Not Found' ]
            ],404);  
        }

        $product->restore();

        return response()->json([
            'product' => $product
        ],200);
    }

    public function add(Request $request, $id)
    {   
        $product = Product::find($id); 

        if($product === null ){ //count($user)
            return response()->json([
                'errors' => [ 'product' => 'Not Found' ]
            ],404);  
        }

        $request->validate([
            'quantity' =>  'required|integer|min:1',
        ]);

        $product->stock += $request->quantity;

        $product->save();

        return response()->json([
            'product' => new ProductResource($product)
        ],200);

    }

    public function remove(Request $request, $id)
    {   
        $product = Product::find($id); 

        if($product === null ){
            return response()->json([
                'errors' => [ 'quantity' => 'Not Found' ]
            ],404);  
        }

        $request->validate([
            'quantity' =>  'required|integer|min:1',
        ]);

        if($request->quantity > $product->stock){
            return response()->json([
                'errors' => [ 'quantity' => 'Invalid Quantity ' ]
            ],422);  
        }

        $product->stock -= $request->quantity;

        $product->save();

        return response()->json([
            'product' => new ProductResource($product)
        ],200);

    }
  
}
