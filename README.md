# Laravel API

## 1. Descarga
Descargar el repositorio
```sh
git clone https://gitlab.com/fqbit/api-rest.git <nombre_proyecto>
```
## 2. Instalación
Ubicarse dentro del directorio
```sh
cd <nombre_proyecto>
```
Instalar dependencias
```sh
composer install
```
Crear el archivo .env en base a .env.example
WINDOWS
```sh
copy .env.example .env
```
LINUX - MAC
```sh
cp .env.example .env
```
Generar la llave de la aplicación
```sh
php artisan key:generate
```
## 3. Base de Datos
Configurar la base de datos en el archivo  .env
```sh
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=api
DB_USERNAME=root
DB_PASSWORD=password
```
Ejecutar migraciones y seeders
```sh
php artisan migrate --seed
```
Instalar passport
```sh
php artisan passport:install
```
## 4. Almacenamiento
Generar enlace simbólico
```sh
php artisan storage:link
```
## 5. Iniciar servidor
```sh
php artisan serve
```
Credenciales
```sh
"email":"admin@qbit.mx"
"password":"password"
```


