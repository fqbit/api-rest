<?php

use Illuminate\Support\Facades\Route;
use App\Models\User;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function (){
    $user = User::find(1);
    $user->syncRoles(['admin']);
    $user = User::find(2);
    $user->assignRole('editor');
    $user = User::find(3);
    $user->assignRole('guest');
});